# Generated by Django 2.2.4 on 2019-08-28 15:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailpress', '0005_blogindexpage'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blogindexpage',
            options={'verbose_name': 'Blog index', 'verbose_name_plural': 'Blog index'},
        ),
    ]
