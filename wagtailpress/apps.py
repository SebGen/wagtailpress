from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class WagtailpressConfig(AppConfig):
    name = 'wagtailpress'
    verbose_name = _('WagtailPress')
