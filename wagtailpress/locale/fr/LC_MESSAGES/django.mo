��    -      �  =   �      �     �     �          (     0     6     C     W     l  
   z     �     �     �     �     �     �     �     �     �     �     �     �     	                    4     @     I     Z  	   c     m  	   q     {     �  
   �     �  	   �     �     �     �     �     �     �  A  �          1     N     i     v     {     �  !   �     �     �     �     �                         *     /     A  !   G     i     v     �     �     �  (   �     �     �     �     �     	     	     	     %	     *	     /	     @	     R	     d	     h	     u	     {	     �	     �	               -   	                                  +   !                                $       *          (   
                "   '            &         #      )      ,          %                                  About the author(s) Article corresiponding to tag Article informations Authors Black Blog article Blog article author Blog article authors Blog articles Blog index Blue Bottom Left Bottom Right By Centered Description Green Header level Image Image with text overlay Introduction Latest articles Link Next No tags Only for visitor display Person page Previous Publication date RSS Feed Read More Red Return to Tags Text Text color Top Left Top Right URL WagtailPress White content header image of Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 À propos des auteur·e·(s) Article correspondant au tag Informations sur l'article Auteur·e·s Noir Article de blog Auteur·e d'un article de blog Auteur·e·s d'un article de blog Articles de blog Index de blog Bleu En bas à gauche En bas à droite Par Centré Description Vert Niveau d'en-tête Image Image avec superposition de texte Introduction Derniers articles Link Suivant Pas de tags Seulement pour l'affichage aux visiteurs Page de personne Précédent Date de publication Flux RSS Lire la suite Rouge Retourner à Tags Text Couleur du texte En haut à gauche En haut à droite URL WagtailPress Blanc content image d'en-tête de 